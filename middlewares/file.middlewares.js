const fs = require('fs');
const path = require('path');
const { handlerResponse } = require('../error.handlers');


const fileMiddlewares = {

    doesFileDataExist: (req, res, next) => {
        const { content, filename } = req.body;
        const { url } = req;

        if (!content || !filename) {
            return handlerResponse.errorHandlerClient(res, "Please specify 'content' parameter");
        }

        if (filename.split(".").length <= 1) {
            return handlerResponse.errorHandlerClient(res, "Please specify 'content' parameter");
        }

        const arrayOfFiles = fs.readdirSync(path.join(__dirname, '/../test/'));

        const urlArr = url.split('/');

        if (!(urlArr[urlArr.length - 1] === 'edit')) {
            if (arrayOfFiles.includes(filename)) {
                return handlerResponse.errorHandlerClient(res, "File already exist");
            }
        } else {
            if (!arrayOfFiles.includes(filename)) {
                return handlerResponse.errorHandlerClient(res, "File doesn't exist you cannot edit");
            }
        }

        next();
    },

    doesClientError: (req, res, next) => {
        if (!req) return handlerResponse.errorHandlerClient(res, "Client error");
        next();
    },

    doesUrlExist: (req, res, next) => {
        const { filename } = req.params;


        if (!filename) return handlerResponse.errorHandlerClient(res, "Client error");

        const arrayOfFiles = fs.readdirSync(path.join(__dirname, '/../test/'));

        if (!arrayOfFiles.includes(filename)) {
            return handlerResponse.errorHandlerClient(res, "No file with " + filename + " filename found");
        }

        next();
    }
}

module.exports = {
    fileMiddlewares
}