const { Router } = require('express');
const { filesRouter } = require('./files.router');

const routes = Router();

routes.use('/', filesRouter);

module.exports = { routes };