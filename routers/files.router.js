const { fileController } = require('../controllers/file.controller');
const { fileMiddlewares } = require('../middlewares/file.middlewares');

const { Router } = require('express');

const filesRouter = Router();


filesRouter.post('/files/', fileMiddlewares.doesFileDataExist, fileController.createFile);

filesRouter.get('/files/', fileMiddlewares.doesClientError, fileController.getFiles);

filesRouter.get('/files/:filename', fileMiddlewares.doesUrlExist, fileController.getFile);
filesRouter.put('/files/edit', fileMiddlewares.doesFileDataExist, fileController.createFile);


module.exports = { filesRouter };