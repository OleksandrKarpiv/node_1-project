const handlerResponse = {
    errorHandlerClient: (res, message) => {
        return res.status(400).send(JSON.stringify({ message }));
    },

    errorHandlerServer: (res, message = "Server error") => {
        return res.status(500).send(JSON.stringify({ message }));
    },

    successHandlerServer: (res, message, dataResp = null) => {
        return !dataResp ? res.status(200).send(JSON.stringify({ message })) :
            res.status(200).send(JSON.stringify({ message, ...dataResp }));
    }

}

module.exports = {
    handlerResponse
}