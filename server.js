const { json } = require('express');
const express = require('express')
const cors = require('cors');
const morgan = require('morgan');
const fs = require('fs');
const { routes } = require('./routers/api.routers');


if (!fs.existsSync('./test')) {
    fs.mkdirSync('test');
}

const app = express();
morgan.token('body', req => {
    return JSON.stringify(req.body)
})

app.use(morgan(':method :url :body'));

app.use(cors());
const port = 8080;


app.use(json());
app.use('/api', routes);

app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
});