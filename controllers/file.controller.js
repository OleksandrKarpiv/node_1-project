const fs = require('fs');
const path = require('path');
const { handlerResponse } = require('../error.handlers');
const { filepath } = require('../consant');

const fileController = {
    createFile: (req, res) => {

        const { content, filename } = req.body;

        fs.appendFile(path.join(__dirname, filepath, filename), content, (err) => {
            if (err) handlerResponse.errorHandlerServer(res);
            return handlerResponse.successHandlerServer(res, "File created successfully");
        });
    },

    getFiles: (req, res) => {
        fs.readdir(path.join(__dirname, filepath), (err, files) => {
            if (err) handlerResponse.errorHandlerServer(res);
            return handlerResponse.successHandlerServer(res, "Success", { files });
        });
    },

    getFile: (req, res) => {
        const { filename } = req.params;

        if (!filename) return handlerResponse.errorHandlerServer(res);

        fs.readFile(path.join(__dirname, filepath, filename), 'utf8', (err, data) => {
            if (err) return handlerResponse.errorHandlerServer(res);
            const dat = filename.split('.');
            const ext = dat[dat.length -1];
            fs.stat(path.join(__dirname, filepath, filename), (err, stats) => {
                return handlerResponse.successHandlerServer(res, "Success", {
                    filename,
                    content: data,
                    extension: ext,
                    uploadedDate: stats.ctime
                });

            });
        });
    }

}

module.exports = { fileController };